import { Task } from "../types.ts";
import { v4 } from "https://deno.land/std/uuid/mod.ts";

// class ToDo(){
//     constructor(id,task,status) {
//         this.id = id;
//         this.task = task;
//         this.status = status
//     }
// }

//  it takes hrs and mins as a parameter converts it into 12hr format and returns a string

function convertTo12(hrs: number, min = 0) {
  // TODO: set the time by calling this function from the object

  let time = "",
    num;
  if (hrs > 12 && hrs <= 24) {
    time = String(hrs - 12);
    if (min < 60 && min > -1) {
      time += ":" + String(min) + " PM";
    } else if (min == 60 || min > 60) {
      num = parseInt(time);
      time = String(++num);
      num = min - 60;
      time += ":" + String(num) + " PM";
    }
  } else if (hrs < 12 && hrs > 0) {
    time = String(hrs);
    if (min < 60 && min > -1) {
      time += ":" + String(min) + " AM";
    } else if (min == 60 || min > 60) {
      num = parseInt(time);
      time = String(num++);
      num = min - 60;
      time += ":" + String(num) + " AM";
    } else {
      return "Enter the mins within [0-60] range";
    }
  } else {
    return "Enter the hrs within [1-12] range";
  }
  return time;
}

let toDoList: Task[] = [
  // {
  //     id: "1",
  //     task: "walk the doggo",
  //     date: "12-mar-30",
  //     hour: 16,
  //     min: 35,
  //     time: setTime(this.hour,this.min), //TODO
  //     repeat: true
  // },
  {
    id: "2",
    task: "buy grocessaries",
    time: "06:30PM",
    repeat: false,
  },
  {
    id: "3",
    task: "feed mimi",
    time: "07:00PM",
    repeat: true,
  },
];

// @desc Get all the toDo tasks list
// @route GET /to-do/API/v1/tgetToDoList
const getToDoList = ({ response }: { response: any }) => {
  response.body = {
    success: true,
    data: toDoList
  }
};

// @desc Get a task from the to-do list with a particular ID
// @route GET /to-do/API/v1/task/:id
const getTask = ({ params, response }: { params: { id: string }, response: any }) => {
  const task: Task | undefined = toDoList.find(task => task.id === params.id);
  if (task) {
    response.status = 200
    response.body = {
      success: true,
      data: task
    }
  } else {
    response.status = 404,
      response.body = {
        success: false,
        message: `No task found with id: ${params.id}`
      }
  }
};

// @desc Add a task to the to-do list
// @route POST /to-do/API/v1/tasks
// need to add async to addTask(), because await is no longer in global scope
const addTask = async ({ request, response }: { request: any, response: any }) => {
  const body = await request.body()
  if (!request.hasBody) {
    response.status = 400;
    response.body = {
      success: false,
      message: "No data"
    }
  } else {
    const task: Task = body.value;
    task.id = v4.generate();
    toDoList.push(task);
    response.status = 201;
    response.body = {
      status: true,
      data: task
    }
  }
};

// @desc Update a task in the to-do list
// @route PUT /to-do/API/v1/tasks/:id
// need to add async to updateTask(), because await is no longer in global scope
const updateTask = async ({ params, request, response }: { params: { id: string }, request: any, response: any }) => {

  const task: Task | undefined = toDoList.find(task => task.id === params.id);
  if (task) {
    const body = await request.body();
    const updateTask: { task: string; time: string; repeat: boolean } = body.value;

    /* ******************* TODO : refactor code ******************* 

    // const updateTask: { task?: string; time?: string; repeat?: boolean } = body.value;
    // CAUSE:? optional value
    // changed it to a required parameter -- WORKS

    // let toDoList : Task[] = ;
    // ----> initialized toDoList with an empty array. doesnt work
    // ----> initialized toDoList with the values. doesnt work

    // ERROR: Type 'void[]' is not assignable to type 'Task[]'
   let  updatedToDoList = toDoList.map(t => {
      // t.id === t.id ? {...task, ...updateTask } : task;

      //  or 
      // if(t.id === params.id) {
      //   t.task = updateTask.task;
      //   t.time = updateTask.time;
      //   t.repeat = updateTask.repeat;
      // }
    }); */

    // ERROR: Type 'void[]' is not assignable to type 'Task[]'
    // toDoList = updatedToDoList;

    toDoList.forEach(t => {
      if (t.id === params.id) {
        t.task = updateTask.task;
        t.time = updateTask.time;
        t.repeat = updateTask.repeat;
      }
    });

    response.status = 200;
    response.body = {
      success: true,
      data: toDoList
    }
  } else {
    response.status = 404,
      response.body = {
        success: false,
        message: `Coudn't update.No task found with id: ${params.id}`
      }
  }
};

// @desc Delete a task from the to-do list with a particular ID
// @route DELETE /to-do/API/v1/tasks/:id
const deleteTask = ({ params, response }: { params: { id: string }, response: any }) => {
  
  // console.log( "Inside delete task. input ID : ", params.id );
  // console.log("comparing the current task IDs : input ID");
  // toDoList = toDoList.filter(task => {
  //   task.id !== params.id;
  //   console.log(task.id , ": ", params.id )
  // });
  toDoList.forEach(task => {
    if(task.id === params.id)
    {
      toDoList.splice(toDoList.indexOf(task, 1));
    }
  });
  
  response.body = {
    success: true,
    message: "Task Deleted"
  }
};

export { getToDoList, getTask, addTask, updateTask, deleteTask }