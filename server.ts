import { Application } from "https://deno.land/x/oak/mod.ts";
import router from "./routes.ts";
// const to_do = new
const port = 5000;

const app = new Application();

// for the middleware
app.use(router.routes());

// here methods imply GET, PUT, POST, DELETE
// you can even restrict few methods if needed
app.use(router.allowedMethods());

await app.listen({ port });
