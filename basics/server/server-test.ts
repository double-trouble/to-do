import { serve } from 'https://deno.land/std/http/server.ts'
const s = serve({ port: 8000 })
console.log('http://localhost:8000/')
for await (const req of s) {
  req.respond({ body: 'Hello World\n' })
}

/*
* Here, we import serve object from the serve.ts URL
* call the serve function with an object(port number)
* 
* in the for await loop all the requests returned in the server() function are handled
* for each request  is handled by adding a response
*/