const encoder = new TextEncoder(),
    messageText = encoder.encode("Deno is more secure!");

await Deno.writeFile("message.txt", messageText); 

/* 
* Here, Deno.writeFile() returns a promise. So this needs to be handled with a await.
*    usually, the await functions needs to be written in a async function, which completed other tasks untill the await promise is fulfilled.
*    async function structure:
        async function functionName(){
            let resp = await fetch(URL)
            .then(resp => resp.json())  --reads the response from fetch and converts into .json format
            .then(data => console.log(data))
            .catch(err => console.log(err));
             
            OR using await
            let response = await fetch(URL-2);
            let data = await response.json();
            rconsole.log(data);
        }

* Instead of weapping the await calls in a async function, you can directly use await on the global scope.
*    
* This is one of the reasons, deno.js is faster than node.js
*
*
*
* await Deno.writeFile("message.txt", messageText, {create: false});
* ---- this works only when the file exists.
* ---- requires 'allow-read' flag
*
*
* await Deno.writeFile("message.txt", messageText, {append: true,
 mode : 0o777});
* ---- this works only when the file exists.
* ---- also sets the file permissions to 777.
* ---- requires 'allow-read' flag 
*/