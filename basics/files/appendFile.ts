const appendencoder = new TextEncoder(),
    message = appendencoder.encode("\nDeno is more secure! -- appended message");

await Deno.writeFile("message.txt", message, {append: true });

/*
*   there are two ways to handle files 
*       > synchronously - Deno.writeFileSync("fileName.txt, message, {option: value}")
*       > asynchronously - asynv Deno.writeFile("fileName.txt, message, {option: value}")
*/
