import {
    dayOfYear, currentDayOfYear
} from "https://deno.land/std/datetime/mod.ts"

console.log("secret message: ", dayOfYear(new Date("2020-05-22")));

let date = new Date(),
    year= date.getFullYear(),
    daysInYear= (year % 4 == 0) ? 366 : 365;
    
console.log("Days left to figure it out: ", daysInYear- currentDayOfYear());