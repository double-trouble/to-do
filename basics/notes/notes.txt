WHY DENO? 

    secure
        the permissions needs to be explicitly given.
        deno has no access to 
            file system
            network
            execution of other scripts
            environment variables
    offers a sandbox security layer through PERMISSIONS.
        a program can access the permissions set to the executable as flags by the user.
        while Node.js program can access anything the user has access to.
    decentralized packages
        no node modules and any ES module can be accessed through a URL 
        eg: "https://deno.land/package-name"
        these are then downloaded onto your hard drive 
        no more node-modules!

        The memes about node-modules are gonna be history!
    ES modules
    uses ECMAScript features in all its API and standard libraries
        while node uses callback-based standard library and has no plans to upgrade it.
    Await can be used on a global scope without wrapping in a Async function
    Built-in testing
    browser compatibility API
        fetch 
        window object
    execute Web Assembly(Wasm) Binaries directly

------------------------------------------------
DENO REPL

after installing using curl command: 
    curl -fsSL https://deno.land/x/install/install.sh | sh

add the path of this /.deno/bin folder to the $PATH environmentvariable, by editing the .bashrc file.
add this line to the .bashrc file.
    export PATH="/home/userName/.deno/bin:$PATH"

now you can run the deno repl by typing the command 
    deno

check the version of deno
    deno --version

------------------------------------------------
FETCH THE SCRIPTS USING THE URL : run

instead of the downloading the node-modules to access the packages, deno uses URLs to fetch the package and cache it you hard drive 
    deno run "link/to/the/script"
    deno run https://deno.land/std/examples/welcome.ts

since deno needs permissions to access the system resources, you need to add some FLAGS while using the run command
    deno run --FLAG-1 --FLAG-2 "link/to/the/script"
    deno run --allow-read --allow-net https://deno.land/std/http/file_server.ts

if the flags aren't specified, it results in a "permissionDenied" ERROR
    deno run https://deno.land/std/http/file_server.ts
    error: Uncaught PermissionDenied: read access to "/home/zylith/to-do", run again with the --allow-read flag

    now when run with --allow-read flag, it still gives an error
    deno run --allow-read  https://deno.land/std/http/file_server.ts
    error: Uncaught PermissionDenied: network access to "0.0.0.0:4507", run again with the --allow-net flag   

this starts running the script in the portal :4507
------------------------------------------------
FETCH THE SCRIPTS USING THE URL : install

instead of running a packager, you can insall it. Which stores the package in 
    /home/userName/.deno/bin/packageName

deno install --allow-read --allow-net https://deno.land/std/http/file_server.ts

> now stored in home/userName/.deno/bin/file_server.ts

Now, when you give the command 
    packageName 
    file_server
it starts the localserver and listens at the port 4507