import { Router } from "https://deno.land/x/oak/mod.ts";
import { getToDoList, getTask, addTask, updateTask, deleteTask } from "./controllers/toDo.ts";

const router = new Router();

//endpoints
router.get("/to-do/api/v1/home", ({ response }: { response: any }) => {
  response.body = "get started!";
});

router.get("/todo.list/api/v1/tasks", getToDoList)
  .get("/todo.list/api/v1/task/:id", getTask)
  .post("/todo.list/api/v1/task", addTask)
  .put("/todo.list/api/v1/task/:id", updateTask)
  .delete("/todo.list/api/v1/task/:id", deleteTask);

export default router;
