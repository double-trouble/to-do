export interface Task {
  id: string;
  task: string;
  time: string;
  repeat: boolean;
}
